
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    function is_promise(value) {
        return value && typeof value === 'object' && typeof value.then === 'function';
    }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function prevent_default(fn) {
        return function (event) {
            event.preventDefault();
            // @ts-ignore
            return fn.call(this, event);
        };
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_style(node, key, value, important) {
        node.style.setProperty(key, value, important ? 'important' : '');
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }

    function handle_promise(promise, info) {
        const token = info.token = {};
        function update(type, index, key, value) {
            if (info.token !== token)
                return;
            info.resolved = value;
            let child_ctx = info.ctx;
            if (key !== undefined) {
                child_ctx = child_ctx.slice();
                child_ctx[key] = value;
            }
            const block = type && (info.current = type)(child_ctx);
            let needs_flush = false;
            if (info.block) {
                if (info.blocks) {
                    info.blocks.forEach((block, i) => {
                        if (i !== index && block) {
                            group_outros();
                            transition_out(block, 1, 1, () => {
                                info.blocks[i] = null;
                            });
                            check_outros();
                        }
                    });
                }
                else {
                    info.block.d(1);
                }
                block.c();
                transition_in(block, 1);
                block.m(info.mount(), info.anchor);
                needs_flush = true;
            }
            info.block = block;
            if (info.blocks)
                info.blocks[index] = block;
            if (needs_flush) {
                flush();
            }
        }
        if (is_promise(promise)) {
            const current_component = get_current_component();
            promise.then(value => {
                set_current_component(current_component);
                update(info.then, 1, info.value, value);
                set_current_component(null);
            }, error => {
                set_current_component(current_component);
                update(info.catch, 2, info.error, error);
                set_current_component(null);
            });
            // if we previously had a then/catch block, destroy it
            if (info.current !== info.pending) {
                update(info.pending, 0);
                return true;
            }
        }
        else {
            if (info.current !== info.then) {
                update(info.then, 1, info.value, promise);
                return true;
            }
            info.resolved = promise;
        }
    }

    const globals = (typeof window !== 'undefined'
        ? window
        : typeof globalThis !== 'undefined'
            ? globalThis
            : global);
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if ($$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.21.0' }, detail)));
    }
    function append_dev(target, node) {
        dispatch_dev("SvelteDOMInsert", { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev("SvelteDOMInsert", { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev("SvelteDOMRemove", { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ["capture"] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev("SvelteDOMAddEventListener", { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev("SvelteDOMRemoveEventListener", { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev("SvelteDOMRemoveAttribute", { node, attribute });
        else
            dispatch_dev("SvelteDOMSetAttribute", { node, attribute, value });
    }
    function validate_each_argument(arg) {
        if (typeof arg !== 'string' && !(arg && typeof arg === 'object' && 'length' in arg)) {
            let msg = '{#each} only iterates over array-like objects.';
            if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
                msg += ' You can use a spread to convert this iterable into an array.';
            }
            throw new Error(msg);
        }
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error(`'target' is a required option`);
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn(`Component was already destroyed`); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    /* src/loggedin.svelte generated by Svelte v3.20.1 */

    const { Error: Error_1, console: console_1 } = globals;
    const file = "src/loggedin.svelte";

    // (31:0) {:catch error}
    function create_catch_block(ctx) {
    	let t0;
    	let a;

    	const block = {
    		c: function create() {
    			t0 = text("👤\n\t");
    			a = element("a");
    			a.textContent = "Hi, stranger! Log in or register.";
    			attr_dev(a, "href", "#login");
    			add_location(a, file, 32, 1, 677);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t0, anchor);
    			insert_dev(target, a, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(a);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_catch_block.name,
    		type: "catch",
    		source: "(31:0) {:catch error}",
    		ctx
    	});

    	return block;
    }

    // (28:0) {:then result}
    function create_then_block(ctx) {
    	let t0;
    	let a;
    	let t1;
    	let t2_value = /*result*/ ctx[5] + "";
    	let t2;
    	let t3;

    	const block = {
    		c: function create() {
    			t0 = text("👤\n\t");
    			a = element("a");
    			t1 = text("Hi, ");
    			t2 = text(t2_value);
    			t3 = text("!");
    			attr_dev(a, "href", "#account");
    			attr_dev(a, "title", "Account settings");
    			add_location(a, file, 29, 1, 595);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, t0, anchor);
    			insert_dev(target, a, anchor);
    			append_dev(a, t1);
    			append_dev(a, t2);
    			append_dev(a, t3);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(a);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_then_block.name,
    		type: "then",
    		source: "(28:0) {:then result}",
    		ctx
    	});

    	return block;
    }

    // (27:16)  {:then result}
    function create_pending_block(ctx) {
    	const block = { c: noop, m: noop, p: noop, d: noop };

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_pending_block.name,
    		type: "pending",
    		source: "(27:16)  {:then result}",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let await_block_anchor;
    	let promise_1;

    	let info = {
    		ctx,
    		current: null,
    		token: null,
    		pending: create_pending_block,
    		then: create_then_block,
    		catch: create_catch_block,
    		value: 5,
    		error: 6
    	};

    	handle_promise(promise_1 = /*promise*/ ctx[0], info);

    	const block = {
    		c: function create() {
    			await_block_anchor = empty();
    			info.block.c();
    		},
    		l: function claim(nodes) {
    			throw new Error_1("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, await_block_anchor, anchor);
    			info.block.m(target, info.anchor = anchor);
    			info.mount = () => await_block_anchor.parentNode;
    			info.anchor = await_block_anchor;
    		},
    		p: function update(new_ctx, [dirty]) {
    			ctx = new_ctx;

    			{
    				const child_ctx = ctx.slice();
    				child_ctx[5] = info.resolved;
    				info.block.p(child_ctx, dirty);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(await_block_anchor);
    			info.block.d(detaching);
    			info.token = null;
    			info = null;
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let baseHost = "http://localhost:8080";
    	let basePath = "atlantis";
    	let baseUrl = baseHost + "/" + basePath;
    	let promise = getMe();

    	async function getMe() {
    		let url = baseUrl + "/users/me";
    		console.log(url);
    		console.log("cookie: '" + document.cookie + "'");
    		const response = await fetch(url, { credentials: "include" });
    		console.log(response);
    		const body = await response.json();
    		console.log(body);

    		if (response.ok) {
    			console.log(body.data);
    			return body.data.first_name;
    		} else {
    			throw new Error();
    		}
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1.warn(`<Loggedin> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Loggedin", $$slots, []);

    	$$self.$capture_state = () => ({
    		baseHost,
    		basePath,
    		baseUrl,
    		promise,
    		getMe
    	});

    	$$self.$inject_state = $$props => {
    		if ("baseHost" in $$props) baseHost = $$props.baseHost;
    		if ("basePath" in $$props) basePath = $$props.basePath;
    		if ("baseUrl" in $$props) baseUrl = $$props.baseUrl;
    		if ("promise" in $$props) $$invalidate(0, promise = $$props.promise);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [promise];
    }

    class Loggedin extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Loggedin",
    			options,
    			id: create_fragment.name
    		});
    	}
    }

    /* src/login.svelte generated by Svelte v3.20.1 */

    const { console: console_1$1 } = globals;
    const file$1 = "src/login.svelte";

    function create_fragment$1(ctx) {
    	let form;
    	let label0;
    	let t1;
    	let input0;
    	let t2;
    	let label1;
    	let t4;
    	let input1;
    	let t5;
    	let button;
    	let dispose;

    	const block = {
    		c: function create() {
    			form = element("form");
    			label0 = element("label");
    			label0.textContent = "Email";
    			t1 = space();
    			input0 = element("input");
    			t2 = space();
    			label1 = element("label");
    			label1.textContent = "Password";
    			t4 = space();
    			input1 = element("input");
    			t5 = space();
    			button = element("button");
    			button.textContent = "Log in";
    			attr_dev(label0, "for", "email");
    			add_location(label0, file$1, 46, 2, 1633);
    			input0.required = true;
    			attr_dev(input0, "type", "email");
    			attr_dev(input0, "id", "email");
    			add_location(input0, file$1, 47, 2, 1668);
    			attr_dev(label1, "for", "password");
    			add_location(label1, file$1, 49, 2, 1714);
    			input1.required = true;
    			attr_dev(input1, "type", "password");
    			attr_dev(input1, "id", "password");
    			add_location(input1, file$1, 50, 2, 1755);
    			attr_dev(button, "type", "submit");
    			add_location(button, file$1, 52, 2, 1807);
    			add_location(form, file$1, 45, 0, 1587);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			insert_dev(target, form, anchor);
    			append_dev(form, label0);
    			append_dev(form, t1);
    			append_dev(form, input0);
    			append_dev(form, t2);
    			append_dev(form, label1);
    			append_dev(form, t4);
    			append_dev(form, input1);
    			append_dev(form, t5);
    			append_dev(form, button);
    			if (remount) dispose();
    			dispose = listen_dev(form, "submit", prevent_default(/*doAuth*/ ctx[0]), false, true, false);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(form);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let baseHost = "http://localhost:8080";
    	let basePath = "atlantis";
    	let baseUrl = baseHost + "/" + basePath;
    	let promise = doAuth();

    	async function doAuth(event) {
    		let email = event.target.email.value;
    		let password = event.target.password.value;
    		console.log(email);

    		// console.log(password);
    		let url = baseUrl + "/auth/authenticate";

    		// let headers = {
    		// 	"content-type": "application/json"
    		// };
    		let payload = JSON.stringify({ email: "email", password: "password" });

    		// debugging
    		console.log(url);

    		// console.log(headers)
    		console.log(payload);

    		// call the authentication endpoint
    		// const response = await fetch(authUrl,{ credentials: "include", method: "POST", headers: { "Accept": "application/json", "Content-Type": "application/json" }, body: JSON.stringify({email: email, password: password, mode: "cookie"})});
    		// const response = await fetch(url,{ credentials: "include", method: "POST", headers: { "Accept": "application/json", "Content-Type": "application/json" }, body: JSON.stringify({email: email, password: password, mode: "cookie"})});
    		// const response = await fetch(url, { credentials: "include", method: "POST", headers: headers, body: payload})});
    		const response = await fetch(url, {
    			credentials: "include",
    			method: "POST",
    			body: payload
    		});

    		console.log(response);
    		const body = await response.json();
    		console.log(body);

    		if (response.ok) {
    			console.log(body.data);
    			return body.data.first_name;
    		} else {
    			// log the authentication error
    			console.log(body.error);

    			return body.error;
    		}
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1$1.warn(`<Login> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Login", $$slots, []);

    	$$self.$capture_state = () => ({
    		baseHost,
    		basePath,
    		baseUrl,
    		promise,
    		doAuth
    	});

    	$$self.$inject_state = $$props => {
    		if ("baseHost" in $$props) baseHost = $$props.baseHost;
    		if ("basePath" in $$props) basePath = $$props.basePath;
    		if ("baseUrl" in $$props) baseUrl = $$props.baseUrl;
    		if ("promise" in $$props) promise = $$props.promise;
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [doAuth];
    }

    class Login extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Login",
    			options,
    			id: create_fragment$1.name
    		});
    	}
    }

    /* src/ace.svelte generated by Svelte v3.20.1 */

    const { console: console_1$2 } = globals;
    const file$2 = "src/ace.svelte";

    function create_fragment$2(ctx) {
    	let script;
    	let script_src_value;
    	let style;
    	let dispose;

    	const block = {
    		c: function create() {
    			script = element("script");
    			style = element("style");
    			style.textContent = "#editor {\n\t\t\tmargin: 0;\n\t\t\tposition: absolute;\n\t\t\ttop: 0;\n\t\t\tbottom: 0;\n\t\t\tleft: 0;\n\t\t\tright: 0;\n\t\t}";
    			if (script.src !== (script_src_value = "https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.11/ace.js")) attr_dev(script, "src", script_src_value);
    			attr_dev(script, "type", "text/javascript");
    			attr_dev(script, "charset", "utf-8");
    			add_location(script, file$2, 70, 1, 3877);
    			attr_dev(style, "type", "text/css");
    			attr_dev(style, "media", "screen");
    			add_location(style, file$2, 72, 1, 4019);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor, remount) {
    			append_dev(document.head, script);
    			append_dev(document.head, style);
    			if (remount) dispose();
    			dispose = listen_dev(script, "load", initAce, false, false, false);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			detach_dev(script);
    			detach_dev(style);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    async function initAce() {
    	console.log("loaded ace script");

    	// console.log(ace);
    	console.log("initialising ace editor");

    	var editor = ace.edit("editor");

    	// console.log(editor);
    	editor.setOptions({
    		// editor options
    		selectionStyle: "line", // "line"|"text"
    		highlightActiveLine: true, // boolean
    		highlightSelectedWord: true, // boolean
    		readOnly: false, // boolean: true if read only
    		cursorStyle: "ace", // "ace"|"slim"|"smooth"|"wide"
    		mergeUndoDeltas: true, // false|true|"always"
    		behavioursEnabled: true, // boolean: true if enable custom behaviours
    		wrapBehavioursEnabled: true, // boolean
    		autoScrollEditorIntoView: undefined, // boolean: this is needed if editor is inside scrollable page
    		keyboardHandler: "ace/keyboard/vim", // function: handle custom keyboard events
    		// renderer options
    		animatedScroll: false, // boolean: true if scroll should be animated
    		displayIndentGuides: false, // boolean: true if the indent should be shown. See 'showInvisibles'
    		showInvisibles: false, // boolean -> displayIndentGuides: true if show the invisible tabs/spaces in indents
    		showPrintMargin: true, // boolean: true if show the vertical print margin
    		printMarginColumn: 80, // number: number of columns for vertical print margin
    		printMargin: undefined, // boolean | number: showPrintMargin | printMarginColumn
    		showGutter: true, // boolean: true if show line gutter
    		fadeFoldWidgets: false, // boolean: true if the fold lines should be faded
    		showFoldWidgets: true, // boolean: true if the fold lines should be shown ?
    		showLineNumbers: true,
    		highlightGutterLine: false, // boolean: true if the gutter line should be highlighted
    		hScrollBarAlwaysVisible: false, // boolean: true if the horizontal scroll bar should be shown regardless
    		vScrollBarAlwaysVisible: false, // boolean: true if the vertical scroll bar should be shown regardless
    		fontSize: 16, // number | string: set the font size to this many pixels
    		fontFamily: "fira code", // string: set the font-family css value
    		maxLines: undefined, // number: set the maximum lines possible. This will make the editor height changes
    		minLines: undefined, // number: set the minimum lines possible. This will make the editor height changes
    		maxPixelHeight: 0, // number -> maxLines: set the maximum height in pixel, when 'maxLines' is defined.
    		scrollPastEnd: 0, // number -> !maxLines: if positive, user can scroll pass the last line and go n * editorHeight more distance
    		fixedWidthGutter: false, // boolean: true if the gutter should be fixed width
    		theme: "ace/theme/nord_dark", // theme string from ace/theme or custom?
    		// mouseHandler options
    		scrollSpeed: 2, // number: the scroll speed index
    		dragDelay: 0, // number: the drag delay before drag starts. it's 150ms for mac by default
    		dragEnabled: true, // boolean: enable dragging
    		focusTimout: 0, // number: the focus delay before focus starts.
    		tooltipFollowsMouse: true, // boolean: true if the gutter tooltip should follow mouse
    		// session options
    		firstLineNumber: 1, // number: the line number in first line
    		overwrite: false, // boolean
    		newLineMode: "auto", // "auto" | "unix" | "windows"
    		useWorker: true, // boolean: true if use web worker for loading scripts
    		useSoftTabs: true, // boolean: true if we want to use spaces than tabs
    		tabSize: 4, // number
    		wrap: false, // boolean | string | number: true/'free' means wrap instead of horizontal scroll, false/'off' means horizontal scroll instead of wrap, and number means number of column before wrap. -1 means wrap at print margin
    		indentedSoftWrap: true, // boolean
    		foldStyle: "markbegin", // enum: 'manual'/'markbegin'/'markbeginend'.
    		mode: "ace/mode/markdown", // string: path to language mode
    		
    	});
    }

    function instance$2($$self, $$props, $$invalidate) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1$2.warn(`<Ace> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Ace", $$slots, []);
    	$$self.$capture_state = () => ({ onMount, initAce });
    	return [];
    }

    class Ace extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Ace",
    			options,
    			id: create_fragment$2.name
    		});
    	}
    }

    /* src/diagrams.svelte generated by Svelte v3.20.1 */

    const { Error: Error_1$1, console: console_1$3 } = globals;
    const file$3 = "src/diagrams.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[7] = list[i];
    	return child_ctx;
    }

    // (48:0) {:catch error}
    function create_catch_block$1(ctx) {
    	let p;
    	let t0;
    	let t1_value = /*error*/ ctx[6].message + "";
    	let t1;

    	const block = {
    		c: function create() {
    			p = element("p");
    			t0 = text("Problem occurred fetching diagrams: ");
    			t1 = text(t1_value);
    			set_style(p, "color", "red");
    			add_location(p, file$3, 48, 1, 1068);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    			append_dev(p, t0);
    			append_dev(p, t1);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_catch_block$1.name,
    		type: "catch",
    		source: "(48:0) {:catch error}",
    		ctx
    	});

    	return block;
    }

    // (40:0) {:then diagrams}
    function create_then_block$1(ctx) {
    	let ul;
    	let each_value = /*diagrams*/ ctx[5];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			ul = element("ul");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			add_location(ul, file$3, 40, 1, 923);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, ul, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(ul, null);
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*promise*/ 1) {
    				each_value = /*diagrams*/ ctx[5];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(ul, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(ul);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_then_block$1.name,
    		type: "then",
    		source: "(40:0) {:then diagrams}",
    		ctx
    	});

    	return block;
    }

    // (42:2) {#each diagrams as diagram}
    function create_each_block(ctx) {
    	let li;
    	let a;
    	let t0_value = /*diagram*/ ctx[7].name + "";
    	let t0;
    	let a_href_value;
    	let t1;

    	const block = {
    		c: function create() {
    			li = element("li");
    			a = element("a");
    			t0 = text(t0_value);
    			t1 = space();
    			attr_dev(a, "target", "_blank");
    			attr_dev(a, "href", a_href_value = "#" + /*diagram*/ ctx[7].id);
    			add_location(a, file$3, 43, 3, 968);
    			add_location(li, file$3, 42, 2, 960);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, li, anchor);
    			append_dev(li, a);
    			append_dev(a, t0);
    			append_dev(li, t1);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(li);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(42:2) {#each diagrams as diagram}",
    		ctx
    	});

    	return block;
    }

    // (38:16)   <p>...waiting</p> {:then diagrams}
    function create_pending_block$1(ctx) {
    	let p;

    	const block = {
    		c: function create() {
    			p = element("p");
    			p.textContent = "...waiting";
    			add_location(p, file$3, 38, 1, 887);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, p, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(p);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_pending_block$1.name,
    		type: "pending",
    		source: "(38:16)   <p>...waiting</p> {:then diagrams}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$3(ctx) {
    	let await_block_anchor;
    	let promise_1;

    	let info = {
    		ctx,
    		current: null,
    		token: null,
    		pending: create_pending_block$1,
    		then: create_then_block$1,
    		catch: create_catch_block$1,
    		value: 5,
    		error: 6
    	};

    	handle_promise(promise_1 = /*promise*/ ctx[0], info);

    	const block = {
    		c: function create() {
    			await_block_anchor = empty();
    			info.block.c();
    		},
    		l: function claim(nodes) {
    			throw new Error_1$1("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, await_block_anchor, anchor);
    			info.block.m(target, info.anchor = anchor);
    			info.mount = () => await_block_anchor.parentNode;
    			info.anchor = await_block_anchor;
    		},
    		p: function update(new_ctx, [dirty]) {
    			ctx = new_ctx;

    			{
    				const child_ctx = ctx.slice();
    				child_ctx[5] = info.resolved;
    				info.block.p(child_ctx, dirty);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(await_block_anchor);
    			info.block.d(detaching);
    			info.token = null;
    			info = null;
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let baseHost = "http://localhost:8080";
    	let basePath = "atlantis";
    	let baseUrl = baseHost + "/" + basePath;
    	let promise = getDiagrams();

    	async function getDiagrams(event) {
    		let url = baseUrl + "/items/diagrams";
    		console.log(url);

    		// call the diagrams endpoint
    		const response = await fetch(url, { credentials: "include" });

    		console.log(response);
    		const body = await response.json();
    		console.log(body);

    		if (response.ok) {
    			console.log(body.data);
    			console.log("number of diagrams: " + body.data.length);

    			// console.log(document.getElementById("editor").innerHTML);
    			console.log(editor.value);

    			return body.data;
    		} else {
    			if (body.error) {
    				if (body.error.code == 3) {
    					return "Please log in.";
    				} else {
    					return body.error;
    				}
    			} else {
    				throw new Error("An error occurred.");
    			}
    		}
    	}

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1$3.warn(`<Diagrams> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("Diagrams", $$slots, []);

    	$$self.$capture_state = () => ({
    		baseHost,
    		basePath,
    		baseUrl,
    		promise,
    		getDiagrams
    	});

    	$$self.$inject_state = $$props => {
    		if ("baseHost" in $$props) baseHost = $$props.baseHost;
    		if ("basePath" in $$props) basePath = $$props.basePath;
    		if ("baseUrl" in $$props) baseUrl = $$props.baseUrl;
    		if ("promise" in $$props) $$invalidate(0, promise = $$props.promise);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [promise];
    }

    class Diagrams extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Diagrams",
    			options,
    			id: create_fragment$3.name
    		});
    	}
    }

    /* src/app.svelte generated by Svelte v3.20.1 */
    const file$4 = "src/app.svelte";

    function create_fragment$4(ctx) {
    	let br0;
    	let br1;
    	let br2;
    	let br3;
    	let br4;
    	let br5;
    	let br6;
    	let br7;
    	let br8;
    	let br9;
    	let br10;
    	let br11;
    	let br12;
    	let br13;
    	let br14;
    	let t0;
    	let br15;
    	let br16;
    	let br17;
    	let br18;
    	let br19;
    	let br20;
    	let br21;
    	let br22;
    	let br23;
    	let br24;
    	let br25;
    	let br26;
    	let br27;
    	let br28;
    	let br29;
    	let t1;
    	let nav0;
    	let ul0;
    	let li0;
    	let a0;
    	let t2;
    	let h1;
    	let t4;
    	let h21;
    	let t5;
    	let h20;
    	let t6;
    	let li1;
    	let a1;
    	let t8;
    	let li2;
    	let a2;
    	let t10;
    	let li3;
    	let a3;
    	let t12;
    	let nav1;
    	let ul1;
    	let li4;
    	let t13;
    	let section0;
    	let h30;
    	let t15;
    	let h31;
    	let a4;
    	let t17;
    	let input;
    	let t18;
    	let a5;
    	let t20;
    	let section1;
    	let div0;
    	let t22;
    	let select0;
    	let option0;
    	let option1;
    	let option2;
    	let t26;
    	let pre;
    	let t28;
    	let t29;
    	let section2;
    	let div1;
    	let t31;
    	let select1;
    	let option3;
    	let t33;
    	let div2;
    	let t35;
    	let h22;
    	let t37;
    	let t38;
    	let h23;
    	let t40;
    	let current;
    	const loggedin = new Loggedin({ $$inline: true });
    	const ace = new Ace({ $$inline: true });
    	const login = new Login({ $$inline: true });
    	const diagrams = new Diagrams({ $$inline: true });

    	const block = {
    		c: function create() {
    			br0 = element("br");
    			br1 = element("br");
    			br2 = element("br");
    			br3 = element("br");
    			br4 = element("br");
    			br5 = element("br");
    			br6 = element("br");
    			br7 = element("br");
    			br8 = element("br");
    			br9 = element("br");
    			br10 = element("br");
    			br11 = element("br");
    			br12 = element("br");
    			br13 = element("br");
    			br14 = element("br");
    			t0 = space();
    			br15 = element("br");
    			br16 = element("br");
    			br17 = element("br");
    			br18 = element("br");
    			br19 = element("br");
    			br20 = element("br");
    			br21 = element("br");
    			br22 = element("br");
    			br23 = element("br");
    			br24 = element("br");
    			br25 = element("br");
    			br26 = element("br");
    			br27 = element("br");
    			br28 = element("br");
    			br29 = element("br");
    			t1 = space();
    			nav0 = element("nav");
    			ul0 = element("ul");
    			li0 = element("li");
    			a0 = element("a");
    			t2 = text("🔱\n\t\t\t");
    			h1 = element("h1");
    			h1.textContent = "Atlantis";
    			t4 = space();
    			h21 = element("h2");
    			t5 = text("Diagramming for people in a hurry.");
    			h20 = element("h2");
    			t6 = space();
    			li1 = element("li");
    			a1 = element("a");
    			a1.textContent = "Features";
    			t8 = space();
    			li2 = element("li");
    			a2 = element("a");
    			a2.textContent = "Pricing";
    			t10 = space();
    			li3 = element("li");
    			a3 = element("a");
    			a3.textContent = "About";
    			t12 = space();
    			nav1 = element("nav");
    			ul1 = element("ul");
    			li4 = element("li");
    			create_component(loggedin.$$.fragment);
    			t13 = space();
    			section0 = element("section");
    			h30 = element("h3");
    			h30.textContent = "My cool flowchart";
    			t15 = space();
    			h31 = element("h3");
    			a4 = element("a");
    			a4.textContent = "↙️./";
    			t17 = space();
    			input = element("input");
    			t18 = space();
    			a5 = element("a");
    			a5.textContent = "↙️ v44";
    			t20 = space();
    			section1 = element("section");
    			div0 = element("div");
    			div0.textContent = "⚙️";
    			t22 = space();
    			select0 = element("select");
    			option0 = element("option");
    			option0.textContent = "Mermaid - Flow Chart v1";
    			option1 = element("option");
    			option1.textContent = "Mermaid - Sequence Diagram v1";
    			option2 = element("option");
    			option2.textContent = "Mermaid - Pie Chart v1";
    			t26 = space();
    			pre = element("pre");
    			pre.textContent = "# Hello\n## World\nI like _peanuts_.";
    			t28 = space();
    			create_component(ace.$$.fragment);
    			t29 = space();
    			section2 = element("section");
    			div1 = element("div");
    			div1.textContent = "⚙️";
    			t31 = space();
    			select1 = element("select");
    			option3 = element("option");
    			option3.textContent = "PNG";
    			t33 = space();
    			div2 = element("div");
    			div2.textContent = "(full image preview/output here)";
    			t35 = space();
    			h22 = element("h2");
    			h22.textContent = "Log in";
    			t37 = space();
    			create_component(login.$$.fragment);
    			t38 = space();
    			h23 = element("h2");
    			h23.textContent = "Get diagrams";
    			t40 = space();
    			create_component(diagrams.$$.fragment);
    			add_location(br0, file$4, 7, 0, 172);
    			add_location(br1, file$4, 7, 6, 178);
    			add_location(br2, file$4, 7, 12, 184);
    			add_location(br3, file$4, 7, 18, 190);
    			add_location(br4, file$4, 7, 24, 196);
    			add_location(br5, file$4, 7, 30, 202);
    			add_location(br6, file$4, 7, 36, 208);
    			add_location(br7, file$4, 7, 42, 214);
    			add_location(br8, file$4, 7, 48, 220);
    			add_location(br9, file$4, 7, 54, 226);
    			add_location(br10, file$4, 7, 60, 232);
    			add_location(br11, file$4, 7, 66, 238);
    			add_location(br12, file$4, 7, 72, 244);
    			add_location(br13, file$4, 7, 78, 250);
    			add_location(br14, file$4, 7, 84, 256);
    			add_location(br15, file$4, 8, 0, 263);
    			add_location(br16, file$4, 8, 6, 269);
    			add_location(br17, file$4, 8, 12, 275);
    			add_location(br18, file$4, 8, 18, 281);
    			add_location(br19, file$4, 8, 24, 287);
    			add_location(br20, file$4, 8, 30, 293);
    			add_location(br21, file$4, 8, 36, 299);
    			add_location(br22, file$4, 8, 42, 305);
    			add_location(br23, file$4, 8, 48, 311);
    			add_location(br24, file$4, 8, 54, 317);
    			add_location(br25, file$4, 8, 60, 323);
    			add_location(br26, file$4, 8, 66, 329);
    			add_location(br27, file$4, 8, 72, 335);
    			add_location(br28, file$4, 8, 78, 341);
    			add_location(br29, file$4, 8, 84, 347);
    			add_location(h1, file$4, 14, 3, 409);
    			attr_dev(a0, "href", "/");
    			add_location(a0, file$4, 13, 3, 391);
    			add_location(h20, file$4, 16, 41, 476);
    			add_location(h21, file$4, 16, 3, 438);
    			add_location(li0, file$4, 12, 2, 383);
    			attr_dev(a1, "href", "#features");
    			add_location(a1, file$4, 18, 6, 495);
    			add_location(li1, file$4, 18, 2, 491);
    			attr_dev(a2, "href", "#pricing");
    			add_location(a2, file$4, 19, 6, 539);
    			add_location(li2, file$4, 19, 2, 535);
    			attr_dev(a3, "href", "#about");
    			add_location(a3, file$4, 20, 6, 581);
    			add_location(li3, file$4, 20, 2, 577);
    			add_location(ul0, file$4, 11, 1, 376);
    			attr_dev(nav0, "id", "nav-main");
    			add_location(nav0, file$4, 10, 0, 355);
    			add_location(li4, file$4, 26, 2, 659);
    			add_location(ul1, file$4, 25, 1, 652);
    			attr_dev(nav1, "id", "nav-account");
    			add_location(nav1, file$4, 24, 0, 628);
    			set_style(h30, "display", "none");
    			add_location(h30, file$4, 33, 1, 724);
    			attr_dev(a4, "href", "#picker");
    			attr_dev(a4, "title", "All documents");
    			add_location(a4, file$4, 36, 2, 782);
    			attr_dev(input, "type", "text");
    			input.value = "My cool flowchart";
    			add_location(input, file$4, 37, 2, 833);
    			attr_dev(a5, "href", "#picker");
    			attr_dev(a5, "title", "All versions");
    			add_location(a5, file$4, 38, 2, 883);
    			add_location(h31, file$4, 35, 1, 775);
    			attr_dev(section0, "id", "title");
    			add_location(section0, file$4, 32, 0, 702);
    			attr_dev(div0, "class", "controls");
    			set_style(div0, "display", "inline-block");
    			add_location(div0, file$4, 43, 1, 1051);
    			option0.__value = "mermaid_flow_chart";
    			option0.value = option0.__value;
    			add_location(option0, file$4, 45, 2, 1139);
    			option1.__value = "mermaid_flow_chart";
    			option1.value = option1.__value;
    			add_location(option1, file$4, 46, 2, 1209);
    			option2.__value = "mermaid_flow_chart";
    			option2.value = option2.__value;
    			add_location(option2, file$4, 47, 2, 1285);
    			attr_dev(select0, "class", "format");
    			add_location(select0, file$4, 44, 1, 1113);
    			attr_dev(pre, "id", "editor");
    			add_location(pre, file$4, 49, 1, 1364);
    			attr_dev(section1, "id", "source");
    			set_style(section1, "margin", "0");
    			set_style(section1, "width", "50%");
    			set_style(section1, "display", "inline-block");
    			set_style(section1, "border", "1px solid red");
    			add_location(section1, file$4, 42, 0, 952);
    			attr_dev(div1, "class", "controls");
    			set_style(div1, "display", "inline-block");
    			add_location(div1, file$4, 57, 1, 1521);
    			option3.__value = "png";
    			option3.value = option3.__value;
    			add_location(option3, file$4, 59, 2, 1609);
    			attr_dev(select1, "class", "format");
    			add_location(select1, file$4, 58, 1, 1583);
    			attr_dev(div2, "id", "preview");
    			set_style(div2, "padding", "20px");
    			set_style(div2, "border", "2px dashed black");
    			add_location(div2, file$4, 61, 1, 1654);
    			attr_dev(section2, "id", "output");
    			set_style(section2, "margin", "0");
    			set_style(section2, "width", "50%");
    			set_style(section2, "border", "1px solid blue");
    			add_location(section2, file$4, 56, 0, 1444);
    			add_location(h22, file$4, 66, 0, 1777);
    			add_location(h23, file$4, 70, 0, 1804);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, br0, anchor);
    			insert_dev(target, br1, anchor);
    			insert_dev(target, br2, anchor);
    			insert_dev(target, br3, anchor);
    			insert_dev(target, br4, anchor);
    			insert_dev(target, br5, anchor);
    			insert_dev(target, br6, anchor);
    			insert_dev(target, br7, anchor);
    			insert_dev(target, br8, anchor);
    			insert_dev(target, br9, anchor);
    			insert_dev(target, br10, anchor);
    			insert_dev(target, br11, anchor);
    			insert_dev(target, br12, anchor);
    			insert_dev(target, br13, anchor);
    			insert_dev(target, br14, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, br15, anchor);
    			insert_dev(target, br16, anchor);
    			insert_dev(target, br17, anchor);
    			insert_dev(target, br18, anchor);
    			insert_dev(target, br19, anchor);
    			insert_dev(target, br20, anchor);
    			insert_dev(target, br21, anchor);
    			insert_dev(target, br22, anchor);
    			insert_dev(target, br23, anchor);
    			insert_dev(target, br24, anchor);
    			insert_dev(target, br25, anchor);
    			insert_dev(target, br26, anchor);
    			insert_dev(target, br27, anchor);
    			insert_dev(target, br28, anchor);
    			insert_dev(target, br29, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, nav0, anchor);
    			append_dev(nav0, ul0);
    			append_dev(ul0, li0);
    			append_dev(li0, a0);
    			append_dev(a0, t2);
    			append_dev(a0, h1);
    			append_dev(li0, t4);
    			append_dev(li0, h21);
    			append_dev(h21, t5);
    			append_dev(h21, h20);
    			append_dev(ul0, t6);
    			append_dev(ul0, li1);
    			append_dev(li1, a1);
    			append_dev(ul0, t8);
    			append_dev(ul0, li2);
    			append_dev(li2, a2);
    			append_dev(ul0, t10);
    			append_dev(ul0, li3);
    			append_dev(li3, a3);
    			insert_dev(target, t12, anchor);
    			insert_dev(target, nav1, anchor);
    			append_dev(nav1, ul1);
    			append_dev(ul1, li4);
    			mount_component(loggedin, li4, null);
    			insert_dev(target, t13, anchor);
    			insert_dev(target, section0, anchor);
    			append_dev(section0, h30);
    			append_dev(section0, t15);
    			append_dev(section0, h31);
    			append_dev(h31, a4);
    			append_dev(h31, t17);
    			append_dev(h31, input);
    			append_dev(h31, t18);
    			append_dev(h31, a5);
    			insert_dev(target, t20, anchor);
    			insert_dev(target, section1, anchor);
    			append_dev(section1, div0);
    			append_dev(section1, t22);
    			append_dev(section1, select0);
    			append_dev(select0, option0);
    			append_dev(select0, option1);
    			append_dev(select0, option2);
    			append_dev(section1, t26);
    			append_dev(section1, pre);
    			append_dev(section1, t28);
    			mount_component(ace, section1, null);
    			insert_dev(target, t29, anchor);
    			insert_dev(target, section2, anchor);
    			append_dev(section2, div1);
    			append_dev(section2, t31);
    			append_dev(section2, select1);
    			append_dev(select1, option3);
    			append_dev(section2, t33);
    			append_dev(section2, div2);
    			insert_dev(target, t35, anchor);
    			insert_dev(target, h22, anchor);
    			insert_dev(target, t37, anchor);
    			mount_component(login, target, anchor);
    			insert_dev(target, t38, anchor);
    			insert_dev(target, h23, anchor);
    			insert_dev(target, t40, anchor);
    			mount_component(diagrams, target, anchor);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(loggedin.$$.fragment, local);
    			transition_in(ace.$$.fragment, local);
    			transition_in(login.$$.fragment, local);
    			transition_in(diagrams.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(loggedin.$$.fragment, local);
    			transition_out(ace.$$.fragment, local);
    			transition_out(login.$$.fragment, local);
    			transition_out(diagrams.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(br0);
    			if (detaching) detach_dev(br1);
    			if (detaching) detach_dev(br2);
    			if (detaching) detach_dev(br3);
    			if (detaching) detach_dev(br4);
    			if (detaching) detach_dev(br5);
    			if (detaching) detach_dev(br6);
    			if (detaching) detach_dev(br7);
    			if (detaching) detach_dev(br8);
    			if (detaching) detach_dev(br9);
    			if (detaching) detach_dev(br10);
    			if (detaching) detach_dev(br11);
    			if (detaching) detach_dev(br12);
    			if (detaching) detach_dev(br13);
    			if (detaching) detach_dev(br14);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(br15);
    			if (detaching) detach_dev(br16);
    			if (detaching) detach_dev(br17);
    			if (detaching) detach_dev(br18);
    			if (detaching) detach_dev(br19);
    			if (detaching) detach_dev(br20);
    			if (detaching) detach_dev(br21);
    			if (detaching) detach_dev(br22);
    			if (detaching) detach_dev(br23);
    			if (detaching) detach_dev(br24);
    			if (detaching) detach_dev(br25);
    			if (detaching) detach_dev(br26);
    			if (detaching) detach_dev(br27);
    			if (detaching) detach_dev(br28);
    			if (detaching) detach_dev(br29);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(nav0);
    			if (detaching) detach_dev(t12);
    			if (detaching) detach_dev(nav1);
    			destroy_component(loggedin);
    			if (detaching) detach_dev(t13);
    			if (detaching) detach_dev(section0);
    			if (detaching) detach_dev(t20);
    			if (detaching) detach_dev(section1);
    			destroy_component(ace);
    			if (detaching) detach_dev(t29);
    			if (detaching) detach_dev(section2);
    			if (detaching) detach_dev(t35);
    			if (detaching) detach_dev(h22);
    			if (detaching) detach_dev(t37);
    			destroy_component(login, detaching);
    			if (detaching) detach_dev(t38);
    			if (detaching) detach_dev(h23);
    			if (detaching) detach_dev(t40);
    			destroy_component(diagrams, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;
    	validate_slots("App", $$slots, []);
    	$$self.$capture_state = () => ({ LoggedIn: Loggedin, Login, Ace, Diagrams });
    	return [];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$4, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment$4.name
    		});
    	}
    }

    var app = new App({
    	target: document.body
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
