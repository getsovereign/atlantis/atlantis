# Database Assets

This directory contains Directus collections, sample data, and scripts for backing up and restoring the database.

Directus collections are lightweight wrappers for database tables.

## Updating Directus Collections

- Use API requests (eg. via Postman) to get Directus collections.
- Write the value of the `data` key to a JSON file with the corresponding name.
    - Overwrite the previous file contents.
- Reference the file in the setup script.
