# back up today's content with activity (audit) and revision history

mysqldump --column-statistics=0 -h 127.0.0.1 -u admin -p --single-transaction --quick --lock-tables=false atlantis directus_activity directus_permissions directus_relations directus_revisions directus_roles directus_users elements folders team_managers team_settings user_settings > db/dumps/db-content-history_$(date +%F).sql
