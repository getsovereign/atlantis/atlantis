# back up today's content without audit (activity) and revision history

mysqldump --column-statistics=0 -h 127.0.0.1 -u admin -p --single-transaction --quick --lock-tables=false atlantis directus_permissions directus_relations directus_roles directus_users elements folders team_managers team_settings user_settings > db/dumps/db-content_$(date +%F).sql
