# Welcome to Atlantis 🔱

Atlantis is document editing made easy for individuals and teams. [Use Atlantis](http://useatlantis.com) to edit documents and generate diagrams using markup formats like Markdown and Mermaid.js. For example, you can easily format and preview an article with [CommonMark](https://commonmark.org/), or automatically generate a flow chart with [Mermaid.js](https://mermaidjs.github.io/) syntax. Simply add some light-weight markup and let 🔱 do the rest!

[[_TOC_]]

## About Atlantis

More details coming soon.

## Installing Atlantis

These instructions will take you through setting up the Atlantis app in development and production environments. It assumes a RHEL-based Linux operating system (Amazon Linux 2, in this instance).

Atlantis uses an open-source, headless CMS called [Directus](https://directus.io) for its backend. Directus has official Docker containers which allow for rapid installation and development.

### 1. Install the Dependencies

`sudo yum install docker git python3`
`sudo pip3 install docker-compose requests`

### 2. Clone the Atlantis Repository

`git clone https://gitlab.com/getsovereign/atlantis/atlantis.git`

### 3. Install Atlantis

#### Development Environment

The setup script is at `./<atlantis_dir>/api/setup-dev.sh`.

- Update the setup script with your Directus email and password.
- Ensure your desired setup mode is uncommented and the other one is commented:
    1. Create an empty environment for restoring content
    2. Create an environment with sample users and content for testing collections and permissions
- Ensure the Docker service is running and you're a member of the docker group:
    - `sudo systemctl start docker`
    - `sudo usermod -aG docker ${USER}`
- Run the setup script:
    - `cd <atlantis_dir>`
    - `./api/setup-dev.sh`
- Fix the Directus relationships (atlantis#3).
- Restore content backups using the db scripts, if necessary.

#### Production Environment

TBD

#### Post-Installation Notes

- The Atlantis web interface can be reached at [https://localhost:5000/](http://localhost:5000/).
- The Atlantis admin interface can be reached at [https://localhost:5000/admin/](http://localhost:5000/admin/).

##### Directus Notes

- The Directus API can be reached at [https://localhost:8443/atlantis/](https://localhost:8443/atlantis/). This serves as the backend of the Atlantis app, and will be used exclusively by the web interface, setup script, and other admin tools.
- The Directus admin interface can be reached at [https://localhost:8443/admin/](https://localhost:8443/admin/). This is not needed as Atlantis will provide a web interface for end users and administrators. Even though security measures such as access are in place, it is best to prevent access to this console and only allow interaction via the API.

### Contribution

Atlantis will always remain open-source under the MIT license. Pull requests are welcome!
