# front page notes

free to use, no registration required, to encourage adoption and sharing

- inspired by the google homepage
- the editor is front and centre
    - there are some basic settings on the top
    - share link
        - live preview toggle
            - eventually
        - live preview speed
            - slow, medium, fast
        - dark theme toggle
            - eventually
- anyone can make a document and share it
    - ie. unauthenticated users
- certain features will require an account
    - saving documents?
        - not sure about this yet
        - will probably make it that way at the start
    - saving user settings
    - creating organisations
    - creating teams
    - creating pages
    - etc
- free accounts are very generous
    - export to multiple formats
    - high resolution images
    - etc
