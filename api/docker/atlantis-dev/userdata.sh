sudo yum update -y
sudo yum install -y docker git mariadb python3
sudo pip3 install requests
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
git clone https://gitlab.com/getsovereign/atlantis/atlantis.git
sudo systemctl start docker
cd ~/atlantis/
sudo api/./setup-dev.sh
