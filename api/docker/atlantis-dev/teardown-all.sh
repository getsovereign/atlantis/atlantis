# IMPORTANT NOTE: this will stop **ALL** containers, not just atlantis ones

echo -e "Stopping all containers..."
docker stop $(docker ps -aq)
echo -e "...done\n"
echo -e "Removing all containers..."
docker rm $(docker ps -aq)
echo -e "...done"
