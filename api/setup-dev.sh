# sets up a dev environment

export PATH=$PATH:/usr/local/bin

# pull docker images
echo -e "Pulling Docker images..."
docker-compose -f api/docker/atlantis-dev/docker-compose.yml pull
echo -e "...done\n"

# create docker containers
echo -e "Creating Docker containers..."
docker-compose -f api/docker/atlantis-dev/docker-compose.yml up -d
sleep 30
echo -e "...done\n"

# install directus
echo -e "Installing Directus..."
docker-compose -f api/docker/atlantis-dev/docker-compose.yml run directus install --email admin@useatlantis.local --password HelloWorld!
echo -e "...done\n"

# set up atlantis
echo -e "Setting up Atlantis..."
# create an empty environment for restoring content
api/./setup.py --host https://localhost --port 8443 --email admin@useatlantis.local --password HelloWorld!
# create an environment with sample users and content for testing collections and permissions
# api/./setup.py --host https://localhost --port 8443 --email admin@useatlantis.local --password HelloWorld! --samples true
echo -e "...done\n"
