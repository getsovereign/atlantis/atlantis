#!/usr/bin/env python3

"""
Setup script for a new Atlantis app on the Directus platform.

- Configures new Directus installations with initial settings and content.
- Adds sample data if specified.

Note: this script requires Directus to be installed.

TODO
- switch to proper json logging with a library
- make random element creation for sample data more varied
- fix comments in functions and throughout
"""

import argparse
import json
import logging
import os
import requests
import uuid

######################################################################## globals

# environment properties
env = ""
base_host = ""
base_url = "/atlantis"

# admin credentials (specified externally)
admin_email = ""
admin_password = ""

# path to directus data files
DATA_PATH = "./db"

# role ids for the primary directus roles
DIRECTUS_ADMIN_ROLE_ID = 1
DIRECTUS_PUBLIC_ROLE_ID = 2
DIRECTUS_IND_ROLE_ID = 3

# permission ids for updating the status of directus users and roles
DIRECTUS_ROLES_PERM_ID = 10
DIRECTUS_USERS_PERM_ID = 12
DIRECTUS_USERS_ACTIVE_PERMS_ID = 40
DIRECTUS_USERS_DELETED_PERMS_ID = 41
DIRECTUS_USERS_INVITED_PERMS_ID = 43
DIRECTUS_USERS_SUSPENDED_PERMS_ID = 44

# current jwt bearer token for the authenticated user
token = ""

# current authorization header for the authenticated user
auth_header = {}

# flag for creating sample data
create_sample_data = False

# default password for sample users
DEFAULT_TEST_PASSWORD = "HelloWorld!"

######################################################################## logging

logging.basicConfig(
    format = '{"date":"%(asctime)s", "level":"%(levelname)s",' +
        '"function":"%(funcName)s()", "message":"%(message)s"}',
    datefmt = "%Y-%m-%dT%H:%M:%S%z")
log = logging.getLogger("atlantis")

############################################################################ cli

cli_parser = argparse.ArgumentParser(description="You've found Atlantis CLI!")

def configure_cli_args():
    cli_parser.add_argument("--host", type=str, default="http://localhost",
        required=True, help="specify the host of the Atlantis server")
    cli_parser.add_argument("--port", type=str, default="8080", required=True,
        help="specify the port of the Atlantis server")
    cli_parser.add_argument("--env", type=str, choices=["dev", "prod"],
        default="dev", help="specify the environment type")
    cli_parser.add_argument("--email", type=str, required=True,
        help="specify the admin email")
    cli_parser.add_argument("--password", type=str, required=True,
        help="specify the admin password")
    cli_parser.add_argument("--samples", type=str, choices=["true", "false"],
        default="false", help="generate sample data")
    cli_parser.add_argument('--logging', type=str,
        choices=["debug", "info", "warn", "error"], default="debug",
        help="specify the log level")

########################################################################### init

def init_script():
    global env, base_host, base_url, create_sample_data, \
        admin_email, admin_password

    configure_cli_args()
    user_cli_args = cli_parser.parse_args()

    log.setLevel(user_cli_args.logging.upper())
    log.debug(f"{user_cli_args}")

    base_host = user_cli_args.host

    base_url = "%s:%s%s" % (user_cli_args.host, user_cli_args.port, base_url)
    log.debug(base_url)

    env = user_cli_args.env
    log.debug(env)

    admin_email = user_cli_args.email
    admin_password = user_cli_args.password
    log.debug(admin_email)

    if user_cli_args.samples == "true":
        create_sample_data = True

######################################################################## helpers

def request(action, url, body=None, json=True, auth=True, headers=None):
    """
    Helper: Sends REST API requests via the requests library. Optimises
    arguments based on most requests, which require authentication and have JSON
    payloads. This should be replaced with a (future) Python SDK for Directus.
    """
    if auth == True:
        if headers == None:
            headers = auth_header
        else:
            headers.update(auth_header)

    if json == True:
        if headers == None:
            headers = {"content-type": "application/json"}
        else:
            headers.update({"content-type": "application/json"})

    log.debug(url)
    # these can contain credentials, so this should not be changed to info
    log.debug('{"headers": "%s"}' % headers)
    log.debug(body)

    # we are disabling tls certificate verification
    # TODO: only disable this for dev environments, not production
    # import and suppress the insecure request warning
    from urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

    if action == "get":
        response = requests.get(url, headers=headers, verify=False)
    elif action == "post":
        response = requests.post(url, data=body, headers=headers, verify=False)
    elif action == "patch":
        response = requests.patch(url, data=body, headers=headers, verify=False)
    else:
        raise Exception("invalid request")

    log.debug(response.request.headers)
    response_body = response.json()
    log.debug(response_body)
    if "error" in response_body.keys():
        raise Exception(response_body)

    print("...request complete")
    return response

def authenticate(email, password):
    """
    Helper: Authenticates the provided user then set the global token and auth
    headers.
    """
    global token, auth_header

    url = base_url + "/auth/authenticate"
    body = {
        "email": email,
        "password": password
    }

    response = request("post", url, body, auth=False, json=False)
    token = response.json()["data"]["token"]
    auth_header = {"Authorization": "bearer " + token}

def authenticate_admin():
    print("\n> authenticating admin user...")
    global admin_email, admin_password
    authenticate(admin_email, admin_password)

def get_random_string():
    """
    Helper: Returns a random string using the uuid package.
    """
    return uuid.uuid4().hex.upper()[0:8]

############################################################### directus project

def configure_directus_project():
    """
    Configures the Directus project for the Atlantis app.
    """
    print("\n> updating directus project settings...")
    authenticate_admin()

    url = base_url + "/settings/13"
    body = """{
        "value": "10"
    }"""
    response = request("patch", url)

#################################################################### collections

def create_directus_collections():
    """
    Creates the collections required by the app.
    """
    print("\n> creating collections...")
    collection_names = ["elements", "formats", "folders", "team_managers",
        "team_settings", "user_settings"]

    for collection_name in collection_names:
        print("\n> creating collection %s..." % collection_name)
        url = base_url + "/collections"
        filename = "%s/collections/%s.json" % (DATA_PATH, collection_name)
        collection = open(filename, "rb").read()
        response = request("post", url, body=collection)

######################################################################## formats

def create_formats():
    """
    Creates the default formats for the app.
    """
    print("\n> create formats...")
    format_names = ["markdown", "commonmark"]

    for format_name in format_names:
        print("\n> creating format %s..." % format_name)
        url = base_url + "/items/formats"
        filename = "%s/formats/%s.json" % (DATA_PATH, format_name)
        format = open(filename, "rb").read()
        response = request("post", url, body=format)

########################################################################## roles

def create_default_directus_roles():
    """
    Creates and updates Directus roles required by the app.
    """
    create_role("Individual", "Default role. Individual users can manage" +
        " content, control their subscriptions, and create new teams.")
    update_role(1, "Admins have access to all managed data within the system" +
        " by default. DO NOT USE.")
    update_role(2, "Public, unauthenticated users can register as Directus" +
        " users in a suspended status.")

def create_role(name=None, description=None):
    """
    Creates a role.
    """
    print("\n> creating role %s..." % name)
    url = base_url + "/roles"

    if name == None:
        name = "Team_" + get_random_string()
    if description == None:
        description = "Collaborate on team content, view team subscription" \
            " and settings, see team members, add new team members."

    authenticate_admin()

    body = """{
        "name": "%s",
        "description": "%s"
    }""" % (name, description)
    response = request("post", url, body)
    role_id = response.json()["data"]["id"]
    return role_id

def update_role(id, description):
    """
    Updates a role.
    """
    print("\n> updating role %s..." % id)
    url = base_url + "/roles/%s" % id
    body = """{
        "description": "%s"
    }""" % description
    response = request("patch", url, body)

#################################################################### permissions

def create_default_directus_perms():
    """
    Creates and updates default permissions for Directus roles and users as
    required by the app.
    """
    update_perms_directus_users()
    update_perms_directus_roles()
    create_perms_directus_public_role()
    create_perms_directus_ind_role()

def create_perms(name, body):
    """
    Helper: Creates the permission.
    """
    print("\n> creating permissions %s..." % name)
    url = base_url + "/permissions"
    response = request("post", url, body)

def update_perms(id, name, body):
    """
    Helper: Updates the permission.
    """
    print("\n> updating permissions %s..." % name)
    url = base_url + "/permissions/%s" % id
    response = request("patch", url, body)

def update_perms_directus_users():
    """
    Prevent users from seeing anyone else's user data, by default.
    """
    global DIRECTUS_USERS_PERM_ID
    body = """{
        "read": "mine"
    }"""
    update_perms(DIRECTUS_USERS_PERM_ID, "directus_users", body)

def update_perms_directus_roles():
    """
    Prevent users from seeing other roles/teams, by default.
    """
    global DIRECTUS_ROLES_PERM_ID
    body = """{
        "read": "none"
    }"""
    update_perms(DIRECTUS_ROLES_PERM_ID, "directus_roles", body)

def create_perms_directus_public_role():
    """
    Allow the public to register new Directus users (ie. themselves) in a
    suspended state.
    """
    body = """{
        "collection": "directus_users",
        "role": %s,
        "status": "suspended",
        "create": "full"
    }""" % DIRECTUS_PUBLIC_ROLE_ID
    create_perms("public", body)

######################################################### individual permissions

def create_perms_directus_ind_role():
    """
    Createss the permissions required for individual users.
    """
    create_perms_ind_elements_private()
    create_perms_ind_elements_shared()
    create_perms_ind_elements_public()
    create_perms_ind_elements_deleted()
    create_perms_ind_folders_private()
    create_perms_ind_folders_shared()
    create_perms_ind_folders_public()
    create_perms_ind_folders_deleted()
    create_perms_ind_subscriptions_active()
    create_perms_ind_subscriptions_suspended()
    create_perms_ind_subscriptions_deleted()
    create_perms_ind_user_settings()

def create_perms_ind_formats():
    """
    Creates the permission.
    """
    body = """{
        "collection": "formats",
        "role": %s,
        "status": null,
        "create": "none",
        "read": "full",
        "update": "none",
        "delete": "none",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_formats", body)

def create_perms_ind_elements_private():
    """
    Creates the permission.
    """
    url = base_url + "/permissions"
    body = """{
        "collection": "elements",
        "role": %s,
        "status": "private",
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_elements_private", body)

def create_perms_ind_elements_shared():
    """
    Creates the permission.
    """
    body = """{
        "collection": "elements",
        "role": %s,
        "status": "shared",
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_elements_shared", body)

def create_perms_ind_elements_public():
    """
    Creates the permission.
    """
    body = """{
        "collection": "elements",
        "role": %s,
        "status": "public",
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_elements_public", body)

def create_perms_ind_elements_deleted():
    """
    Creates the permission.
    """
    body = """{
        "collection": "elements",
        "role": %s,
        "status": "deleted",
        "create": "none",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_elements_deleted", body)

def create_perms_ind_folders_private():
    """
    Creates the permission.
    """
    url = base_url + "/permissions"
    body = """{
        "collection": "folders",
        "role": %s,
        "status": "private",
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_folders_private", body)

def create_perms_ind_folders_shared():
    """
    Creates the permission.
    """
    body = """{
        "collection": "folders",
        "role": %s,
        "status": "shared",
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_folders_shared", body)

def create_perms_ind_folders_public():
    """
    Creates the permission.
    """
    body = """{
        "collection": "folders",
        "role": %s,
        "status": "public",
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_folders_public", body)

def create_perms_ind_folders_deleted():
    """
    Creates the permission.
    """
    body = """{
        "collection": "folders",
        "role": %s,
        "status": "deleted",
        "create": "none",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_folders_deleted", body)

def create_perms_ind_subscriptions_active():
    """
    Creates the permission.
    """
    body = """{
        "collection": "subscriptions",
        "role": %s,
        "status": "active",
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_subscriptions_active", body)

def create_perms_ind_subscriptions_suspended():
    """
    Creates the permission.
    """
    body = """{
        "collection": "subscriptions",
        "role": %s,
        "status": "suspended",
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_subscriptions_suspended", body)

def create_perms_ind_subscriptions_deleted():
    """
    Creates the permission.
    """
    body = """{
        "collection": "subscriptions",
        "role": %s,
        "status": "deleted",
        "create": "none",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_subscriptions_deleted", body)

def create_perms_ind_user_settings():
    """
    Creates the permission.
    """
    body = """{
        "collection": "user_settings",
        "role": %s,
        "status": null,
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % DIRECTUS_IND_ROLE_ID
    create_perms("ind_user_settings", body)

############################################################### team permissions

def create_perms_team(role_id):
    """
    Creates the permissions required by teams.
    """
    create_perms_team_formats(role_id)
    create_perms_team_elements_private(role_id)
    create_perms_team_elements_shared(role_id)
    create_perms_team_elements_public(role_id)
    create_perms_team_elements_deleted(role_id)
    create_perms_team_folders_private(role_id)
    create_perms_team_folders_shared(role_id)
    create_perms_team_folders_public(role_id)
    create_perms_team_folders_deleted(role_id)
    create_perms_team_subscriptions_active(role_id)
    create_perms_team_subscriptions_suspended(role_id)
    create_perms_team_subscriptions_deleted(role_id)
    create_perms_team_user_settings(role_id)
    create_perms_team_team_settings(role_id)

def update_perms_team_directus_users(role_id):
    """
    Updates the permission.
    """
    global DIRECTUS_USERS_ACTIVE_PERMS_ID, DIRECTUS_USERS_DELETED_PERMS_ID, \
        DIRECTUS_USERS_INVITED_PERMS_ID, DIRECTUS_USERS_SUSPENDED_PERMS_ID

    body = """{
        "read": "role"
    }"""

    update_perms(DIRECTUS_USERS_ACTIVE_PERMS_ID, "directus_users", body)
    update_perms(DIRECTUS_USERS_DELETED_PERMS_ID, "directus_users", body)
    update_perms(DIRECTUS_USERS_INVITED_PERMS_ID, "directus_users", body)
    update_perms(DIRECTUS_USERS_SUSPENDED_PERMS_ID, "directus_users", body)

def create_perms_team_formats(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "formats",
        "role": %s,
        "status": null,
        "create": "none",
        "read": "full",
        "update": "none",
        "delete": "none",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_formats", body)

def create_perms_team_elements_private(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "elements",
        "role": %s,
        "status": "private",
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_elements_private", body)

def create_perms_team_elements_shared(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "elements",
        "role": %s,
        "status": "shared",
        "create": "full",
        "read": "role",
        "update": "role",
        "delete": "role",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_elements_shared", body)

def create_perms_team_elements_public(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "elements",
        "role": %s,
        "status": "public",
        "create": "full",
        "read": "role",
        "update": "role",
        "delete": "role",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_elements_public", body)

def create_perms_team_elements_deleted(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "elements",
        "role": %s,
        "status": "deleted",
        "create": "none",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_elements_deleted", body)

def create_perms_team_folders_private(role_id):
    """
    Creates the permission.
    """
    url = base_url + "/permissions"
    body = """{
        "collection": "folders",
        "role": %s,
        "status": "private",
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_folders_private", body)

def create_perms_team_folders_shared(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "folders",
        "role": %s,
        "status": "shared",
        "create": "full",
        "read": "role",
        "update": "role",
        "delete": "role",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_folders_shared", body)

def create_perms_team_folders_public(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "folders",
        "role": %s,
        "status": "public",
        "create": "full",
        "read": "role",
        "update": "role",
        "delete": "role",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_folders_public", body)

def create_perms_team_folders_deleted(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "folders",
        "role": %s,
        "status": "deleted",
        "create": "none",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_folders_deleted", body)

def create_perms_team_subscriptions_active(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "subscriptions",
        "role": %s,
        "status": "active",
        "create": "full",
        "read": "role",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_subscriptions_active", body)

def create_perms_team_subscriptions_suspended(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "subscriptions",
        "role": %s,
        "status": "suspended",
        "create": "none",
        "read": "role",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_subscriptions_suspended", body)

def create_perms_team_subscriptions_deleted(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "subscriptions",
        "role": %s,
        "status": "deleted",
        "create": "none",
        "read": "role",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_subscriptions_deleted", body)

def create_perms_team_user_settings(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "user_settings",
        "role": %s,
        "status": null,
        "create": "full",
        "read": "mine",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_user_settings", body)

def create_perms_team_team_settings(role_id):
    """
    Creates the permission.
    """
    body = """{
        "collection": "team_settings",
        "role": %s,
        "status": null,
        "create": "full",
        "read": "role",
        "update": "mine",
        "delete": "mine",
        "comment": "none",
        "explain": "none"
    }""" % role_id
    create_perms("team_team_settings", body)

################################################################### samples data

def create_user(first_name, last_name, email, password, status="active",
    role=DIRECTUS_IND_ROLE_ID):
    url = base_url + "/users"
    body = """{
    	"status": "%s",
    	"role": %d,
    	"first_name": "%s",
    	"last_name": "%s",
    	"email": "%s",
    	"password": "%s"
    }""" % (status, role, first_name, last_name, email, password)
    response = request("post", url, body)

def create_user_settings(key, value):
    url = base_url + "/items/user_settings"
    body = """{
    	"properties": {
    		"%s": "%s"
    	}
    }""" % (key, value)
    response = request("post", url, body)

def create_team(name, description, members):
    role_id = create_role()

    settings = """{
        "team": %s,
        "name": "%s",
        "description": "%s"
    }""" % (role_id, name, description)
    create_team_settings(settings)

    update_perms_team_directus_users(role_id)
    create_perms_team(role_id)

    create_users_content(role_id, members)

def create_team_settings(settings):
    url = base_url + "/items/team_settings"
    response = request("post", url, settings)

def create_users_content(role, users):
    for user in users:
        authenticate_admin()
        create_user(user["first_name"], user["last_name"], user["email"],
            DEFAULT_TEST_PASSWORD, role=role)
        authenticate(user["email"], DEFAULT_TEST_PASSWORD)
        create_user_settings("foo", "bar")
        create_random_element()

def create_element(element):
    url = base_url + "/items/elements"
    response = request("post", url, element)

def create_random_element():
    element = """{
    	"name": "markdown document %s",
    	"description": "example markdown document",
    	"format": 2,
    	"content": "# hello world\\n## a header\\nit is what it is"
    }""" % get_random_string()
    create_element(element)

def create_sample_individuals():
    print("\n> creating example individuals and content...")
    filename = "%s/samples/individuals.json" % DATA_PATH
    users = open(filename, "rb").read()
    create_users_content(DIRECTUS_IND_ROLE_ID, json.loads(users))

def create_sample_team_pied_piper():
    print("\n> creating team pied piper and content...")
    name = "Team Pied Piper"
    description = "The Internet We Deserve"
    filename = "%s/samples/team_pied_piper.json" % DATA_PATH
    users = open(filename, "rb").read()
    create_team(name, description, json.loads(users))

def create_sample_team_hooli():
    print("\n> creating team hooli and content...")
    name = "Team Hooli"
    description = "Making the world a better place"
    filename = "%s/samples/team_hooli.json" % DATA_PATH
    users = open(filename, "rb").read()
    create_team(name, description, json.loads(users))

########################################################################### main

def main():
    """
    Orchestrates the setup of an Atlantis app in an environment.
    """
    print("\n> setting up the atlantis app...\n")

    global env, base_server

    init_script()

    # set up directus project for the atlantis app
    configure_directus_project()
    create_directus_collections()
    create_default_directus_roles()
    create_default_directus_perms()
    create_formats()

    if create_sample_data:
        create_sample_individuals()
        create_sample_team_pied_piper()
        create_sample_team_hooli()

    success_message = """\n> atlantis %s env setup is complete!

        > you can now fix the relationships and restore the previous database.

        > api: %s
        > user interface: %s:5000/
        > admin interface: %s:5000/admin/""" % (env, base_url, \
            base_host, base_host)
    print(success_message)

if __name__ == "__main__":
    main()
